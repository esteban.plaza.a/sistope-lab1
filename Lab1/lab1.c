#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "cabecera.h"
#define READ 0
#define WRITE 1

void lecturaDeInformacionDeDatosAstronomicos(/*char *archivoDeEntrada*/) {
	int i; // vaiable temporal para guardar luego en sscanf todas las posciones
	int cont=0;
	char temp[100];
	FILE *datosAstronomicos;
	datosAstronomicos=fopen("prueba.txt","r");
	char aux[100];
	if(datosAstronomicos == NULL) {
		printf("No se pudo abrir el archivo");
	}
	datosEntrantes *datos1 = (datosEntrantes *)malloc(sizeof(datosEntrantes));
	while(!feof(datosAstronomicos)) // con esto sabré cuantas lineas tiene mi archivo
    {
		fgets(temp,100,datosAstronomicos);
		cont++;
	}
	rewind(datosAstronomicos);// con esto vuelvo al principio del fichero
	i=0;
	while(!feof(datosAstronomicos)) // con este while recorro cada linea hasta que termine el archivo y oy poniendo al mismo tiempo cada cadena de caracteres en su posicion;
	{
		fgets(aux,100,datosAstronomicos);
		sscanf(aux,"%lf %lf %lf %lf %lf",datos1[i].ejeU, datos1[i].ejeV, datos1[i].valorImaginario, datos1[i].valorRuido, datos1[i].valorVisibilidad);
		i++;
    }
	fclose(datosAstronomicos);

	int pipefd[2];
    char buffer[100];
    pipe(pipefd);

    // this child is generating output to the pipe
    //
    int pid;
    pid = fork();
    if (pid == 0) {
        // attach stdout to the left side of pipe
        // and inherit stdin and stdout from parent
        dup2(pipefd[1],STDOUT_FILENO); //STDOUT_FILENO = un int que tiene el descriptor de stdout.
        close(pipefd[0]);              // not using the right side

        execl("hijo", "ls","-al", NULL);
        printf("HOLA\n");
        perror("exec ls failed");
        exit(EXIT_FAILURE);
    }
    else{
        read(pipefd[READ], buffer, 100);
        printf("Padre: %s\n", buffer);
    }


    return EXIT_SUCCESS;
}

